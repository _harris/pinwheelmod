# README #

This project contains the source code needed to run the pinwheel obfuscation technique and the wrapper to generate points constrained to specific regions.

### Prerequisites ###

* PostgreSQL
* PostGIS
* DDLs (see DDL directory)

### How does this work? ###

* The pinwheel technique is a PostgreSQL procedure that interfaces with PostGIS; 
* The wrapper is a query designed to update a result table (defined in the DDLs)
