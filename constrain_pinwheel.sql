-- Example with 45 degree, 1000m radius, this will update the test table.
-- Note that you can replace 45 with any degree and 1000 with any radius

with obs as (
	select ol.location_id, 
		ol.pt_lat,
		ol.pt_lng,
		ST_MakePoint(ol.pt_lng,  ol.pt_lat) as pt,
		dp.pp_pinwheel(45,1000,ST_MakePoint(ol.pt_lng,ol.pt_lat)) as pt_obs
	from dp.pp_pinwheel_1000_omop_location_test_raw_copy ol
	where ol.pt_lng is not null
		and pt_tract <> pt_obs_tract
),
coords as (
	select oa.location_id, 
			oa.pt_lat,
			oa.pt_lng,
			cast(ST_X(oa.pt_obs) as numeric) as pt_obs_lng,
			cast(ST_Y(oa.pt_obs) as numeric) as pt_obs_lat, 
			st_distance(geography(pt), geography(pt_obs)) as st_distance
	from obs oa
), new_obs_pt as (
	select co.*,
			t2.tabblock_id as pt_obs_block,
			substring(t2.tabblock_id from 1 for 12) as pt_obs_blockgroup,
			substring(t2.tabblock_id from 1 for 11) as pt_obs_tract,
			substring(t2.tabblock_id from 1 for 5) as pt_obs_county,
			substring(t2.tabblock_id from 1 for 2) as pt_obs_state
	from coords co
		left join tabblock t2 on ST_Contains(t2.the_geom, st_setsrid(st_point(co.pt_obs_lng, co.pt_obs_lat), 4269))
)
update dp.pp_pinwheel_1000_omop_location lt
set pt_obs_lng = np.pt_obs_lng,
	pt_obs_lat = np.pt_obs_lat,
	st_distance = np.st_distance,
	pt_obs_block = np.pt_obs_block,
	pt_obs_blockgroup = np.pt_obs_blockgroup,
	pt_obs_tract = np.pt_obs_tract,
	pt_obs_county = np.pt_obs_county,
	pt_obs_state = np.pt_obs_state
from new_obs_pt np
where lt.location_id = np.location_id