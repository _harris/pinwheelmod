-- Note that PostGIS is needed for st_project
CREATE OR REPLACE FUNCTION 
	dp.pp_pinwheel (theta numeric, radius numeric, the_geom geometry)
		returns geometry as 
$$
	DECLARE
		angle numeric;
	BEGIN
		angle = random()*360;
		return st_Project(the_geom,mod(CAST(angle as integer), theta)/theta*radius + .01, radians(angle));
	END;
$$
LANGUAGE plpgsql;