-- OMOP is common data model used in healthcare applications, developed by OHDSI.
-- If your data is not in OMOP format, simply download the OMOP DDLs, and convert your data to the expected format.

with obs as (
	select *, 
		ST_MakePoint(ol.longitude,  ol.latitude) as pt,
		dp.pp_pinwheel(45,1000,ST_MakePoint(ol.longitude,ol.latitude)) as pt_obs
	from dp.omop_location ol
	where ol.longitude is not null
),
coords as (
	select oa.location_id, 
			oa.address_1,
			oa.city,
			oa.state,
			oa.zip,
			oa.county,
			oa.latitude as pt_lat,
			oa.longitude as pt_lng,
			cast(ST_X(oa.pt_obs) as numeric) as pt_obs_lng,
			cast(ST_Y(oa.pt_obs) as numeric) as pt_obs_lat, 
			st_distance(geography(pt), geography(pt_obs))
	from obs oa
)
select co.*,
		t1.tabblock_id as pt_block,
		substring(t1.tabblock_id from 1 for 12) as pt_blockgroup,
		substring(t1.tabblock_id from 1 for 11) as pt_tract,
		substring(t1.tabblock_id from 1 for 5) as pt_county,
		substring(t1.tabblock_id from 1 for 2) as pt_state,
		t2.tabblock_id as pt_obs_block,
		substring(t2.tabblock_id from 1 for 12) as pt_obs_blockgroup,
		substring(t2.tabblock_id from 1 for 11) as pt_obs_tract,
		substring(t2.tabblock_id from 1 for 5) as pt_obs_county,
		substring(t2.tabblock_id from 1 for 2) as pt_obs_state
	into dp.pp_pinwheel_1000_omop_location
from coords co
	left join tabblock t1 on ST_Contains(t1.the_geom, ST_SetSRID(ST_Point(co.pt_lng, co.pt_lat), 4269))
	left join tabblock t2 on ST_Contains(t2.the_geom, st_setsrid(st_point(co.pt_obs_lng, co.pt_obs_lat), 4269))