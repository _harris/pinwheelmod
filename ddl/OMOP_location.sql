-- Note that OMOP is a standard and standards may be updated across time

CREATE TABLE dp.omop_location (
	location_id int8 NOT NULL,
	address_1 text NULL,
	address_2 text NULL,
	city text NULL,
	state text NULL,
	zip text NULL,
	county text NULL,
	country text NULL,
	location_source_value text NULL,
	latitude numeric NULL,
	longitude numeric NULL
);